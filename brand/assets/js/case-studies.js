let $cases = $(".case"),
	$gallery = $(".case-gallery-frame"),
	$prev = $(".case-navigation .prev"),
	$next = $(".case-navigation .next"),
	$logos = $(".case-brands li"),
	$bg = $(".case-study-bg > div");

function set_active_case(e) {
	$cases.removeClass("active"), $logos.removeClass("active"), $bg.removeClass("active"), $cases[e].classList.add("active"), $logos[e].classList.add("active"), $bg[e].classList.add("active"), load_gallery(e)
}

function load_gallery(e) {
	let a = $(".case-gallery-item.active");
	a.html(a.html()), $(".case-gallery-item").removeClass("active");
	let t = $("#" + $cases[e].id + " .case-gallery-item"),
		s = $(".case-navigation");
	t.length <= 1 ? s.removeClass("active") : s.addClass("active"), t.length >= 1 ? (t[0].classList.add("active"), $gallery.addClass("active")) : $gallery.removeClass("active")
}
set_active_case(0), $logos.click(function () {
	set_active_case($(this).index())
}), $next.click(function () {
	let e = $(".case-gallery-item.active");
	e.html(e.html());
	let a = $(".case.active .case-gallery-item"),
		t = $(".case.active .case-gallery-item.active").index(),
		s = ++t % a.length < 1 ? 0 : t;
	a.removeClass("active"), a[s].classList.add("active")
}), $prev.click(function () {
	let e = $(".case-gallery-item.active");
	e.html(e.html());
	let a = $(".case.active .case-gallery-item"),
		t = $(".case.active .case-gallery-item.active").index(),
		s = --t < 0 ? a.length - 1 : t;
	a.removeClass("active"), a[s].classList.add("active")
});
/*
var brands = new Swiper(".swiper-container", {
	direction: "horizontal",
	speed: 300,
	updateOnWindowResize: !0,
	breakpoints: {
		0: {
			width: 120,
			centeredSlides: !1
		},
		1200: {
			width: 70,
			centeredSlides: !0
		},
		1440: {
			width: 70,
			centeredSlides: !0
		},
		1920: {
			width: 150,
			centeredSlides: !0
		}
	}
});*/